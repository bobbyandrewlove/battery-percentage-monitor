// Bobby Love
// March 8, 2023
// GNU GPLv3

// Libraries
#include <Wire.h>
#include <Adafruit_GFX.h>
#include "Adafruit_LEDBackpack.h"

// Constants
// Analog input pin for battery.
const int battery = A0;
// Status LED pins.
const int red = 10;
const int yellow = 9;
const int green = 8;

// Variables
// Instantiate display matrix object.
Adafruit_7segment matrix = Adafruit_7segment();
// Stores measured ADC sensor value.
int sensor;
// Stores calculated voltage.
float voltage;
// Stores calculated percentage.
int percentage = 0;
// Stores previous percentage.
int prev;
// Stores time passed since last percentage change.
int timer = 0;

// Setup Function
void setup() {
  // Initialize clock frequency.
  Serial.begin(9600);
  // Initialize display matrix.
  matrix.begin(0x70);
  // Set data pin modes.
  pinMode(battery, INPUT);
  pinMode(red, OUTPUT);
  pinMode(yellow, OUTPUT);
  pinMode(green, OUTPUT);
}

// Main Function
void loop() {
  // Store the previously measured percentage for status update.
  prev = percentage;
  // ADC sensor returns a value ranging from 0 to 1023.
  sensor = analogRead(battery);
  // Convert sensor reading into a voltage value.
  voltage = (float)sensor * (5.0 / 1023.0);
  // Extrapolate percentage to the thousandth of a volt in piecewise fashion.
  // 0%
  if (voltage < 0.394) {
    percentage = 0;
  // 0% to 9%
  } else if (voltage < 2.24) {
    // y = m(x2 - x1) + b
    percentage = 4.876 * (voltage - 0.394);  
  // 9% to 14%
  } else if (voltage < 2.701) {
    percentage = 10.841 * (voltage - 2.24) + 9;
  // 14% to 17%
  } else if (voltage < 2.978) {
    percentage = 10.83 * (voltage - 2.701) + 14;
  // 17% to 20%
  } else if (voltage < 3.07) {
    percentage = 32.538 * (voltage - 2.978) + 17;
  // 20% to 30%
  } else if (voltage < 3.162) {
    percentage = 108.342 * (voltage - 3.07) + 20;
  // 30% to 40%
  } else if (voltage < 3.255) {
    percentage = 107.875 * (voltage - 3.162) + 30;
  // 40% to 70%
  } else if (voltage < 3.347) {
    percentage = 325.027 * (voltage - 3.255) + 40;
  // 70% to 90%
  } else if (voltage < 3.44) {
    percentage = 215.75 * (voltage - 3.347) + 70;
  // 90% to 99%
  } else if (voltage < 3.532) {
    percentage = 97.826 * (voltage - 3.44) + 90;
  // 99%
  } else if (voltage < 3.716) {
    percentage = 99;
  // 100%
  } else {
    percentage = 100;
  }
  // Print percentage to display matrix.
  matrix.print(percentage);
  // Update the display.
  matrix.writeDisplay();
  // Using the battery's status, set the proper LED.
  // If the battery is fully charged, use the green LED.
  if (percentage == 100) {
    digitalWrite(green, HIGH); 
    digitalWrite(yellow, LOW);
    digitalWrite(red, LOW);
  // If the battery will not charge, use the red LED.
  } else if (timer >= 3600) {
    digitalWrite(green, LOW);
    digitalWrite(yellow, LOW);
    digitalWrite(red, HIGH);
  // If the battery is charging, use the yellow LED.
  } else {
    digitalWrite(green, LOW);
    digitalWrite(yellow, HIGH);
    digitalWrite(red, LOW);
  }
  // If the percentage has not changed, increase the timer.
  if (prev == percentage) {
    ++timer;
  // If the percentage has changed, reset the timer.
  } else {
    timer = 0;
  }
  // Wait 5000 milliseconds to save power.
  delay(5000);
 }
